def arrange_data(file_name: str) -> list[str]:
    file = open(file_name, "r")
    return [_.split() for _ in file.readlines()]

def convert_marks_to_int(students: list):
    for index, student in enumerate(students):
        students[index] = [student[0]] + list(map(int, student[1:]))
    return students



def student_ranking(data: list) -> list:
    rankings = []
    rank = []
    curr_sorted_idx = 0
    min_sorted_idx = len(data[0])
    while curr_sorted_idx < len(data) :
        curr_sorted_idx = 0
        for i in range(0, len(data[0]) - 1):
            while curr_sorted_idx < len(data) and sorted(data, key = lambda x: x[i])[curr_sorted_idx] == sorted(data, key = lambda x: x[i + 1])[curr_sorted_idx]:
                curr_sorted_idx += 1
            
            if curr_sorted_idx > 0:
                min_sorted_idx = curr_sorted_idx
                rankings = data[: min_sorted_idx]
                print(curr_sorted_idx)
            data = data[:min_sorted_idx]
                
            rank.append(rankings)
            

        

    return rankings
         
def new_ranking(data: list) -> list:
    data.sort(key = lambda x: x[1])
    overall = []
    rank_list = []
    min_index = 0
    is_valid = True
    for a, b in zip(data, data[1:]):
        is_valid = True
        for i in range(1, len(a)):
            if a[i] > b[i]:
                is_valid = False
            
        
        rank_list.append(a)
        if not is_valid:
            overall.append(rank_list)
            rank_list = []
    overall.append(rank_list)

    return overall
                


print(new_ranking(convert_marks_to_int(arrange_data("student_marks.txt"))))
