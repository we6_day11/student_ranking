from itertools import permutations as nPr
def data(line: str) -> list:
    name, *marks = line.strip().split()
    marks = [int(_) for _ in marks]
    return [name] + marks

def load_data() -> list:
    return [data(line) for line in open("student_marks.txt")]

def check(m,n) -> bool:
    return all(i > j for i, j in zip(m[1:], n[1:]))
student_rank = []
def student_ranking(data_list: list[str, int]):

    data_comb = nPr(data_list, 2)
    for i in data_comb:
        if check(i[0], i[1]):
            student_rank.append(str(i[0][0]) + '>' + str(i[1][0]))
    return student_rank 

print(student_ranking(load_data()))
